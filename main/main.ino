#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

// const char* ssid = "Syberry_Mars";
// const char* password = "Apple35?!";

const char* ssid = "znaharHome";
const char* password = "gthdfz_hjnf";


void HandleRoot();
void HandleCommand();
void FlashLeds();
void TurnOffLeds();
void HandleNotFound();


int analogMotor = 3; // потенциометр подключен к выходу 3


ESP8266WebServer server(80);

// what to do when a client requests "http://<IP>"
void HandleRoot() {
    server.send(200, "text/html", "<html><body><div> \
                                    <div class=\"com_block\">    \
                                    <div class=\"icon\"> \
<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" \
   width=\"512px\" height=\"512px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\"> \
<path d=\"M256,64c-70.688,0-128,57.313-128,128c0,64,64,129,64,192h128c0-65,64-128,64-192C384,121.313,326.688,64,256,64z \
   M323.594,272.906c-12.75,25.344-25.781,51.344-31.938,79.094h-71.469c-6.156-26.906-18.906-52.5-31.375-77.5 \
  C174,244.719,160,216.594,160,192c0-52.938,43.063-96,96-96s96,43.063,96,96C352,216.313,338.188,243.813,323.594,272.906z M320,464 \
  c0,8.844-7.156,16-16,16h-2.938c-6.594,18.625-24.188,32-45.063,32s-38.469-13.375-45.063-32H208c-8.844,0-16-7.156-16-16 \
  s7.156-16,16-16h96C312.844,448,320,455.156,320,464z M320,416c0,8.844-7.156,16-16,16h-96c-8.844,0-16-7.156-16-16s7.156-16,16-16 \
  h96C312.844,400,320,407.156,320,416z M97.719,82.125l28.813,16.656c-6.25,8.656-11.656,17.844-16.094,27.656l-28.719-16.563 \
  L97.719,82.125z M256,32c-5.5,0-10.656,1.063-16,1.625V0h32v33.625C266.656,33.063,261.5,32,256,32z M162.781,62.531l-16.656-28.813 \
  l27.75-16l16.563,28.719C180.625,50.875,171.438,56.281,162.781,62.531z M401.594,126.438c-4.438-9.813-9.875-19-16.125-27.656 \
  l28.813-16.656l16,27.75L401.594,126.438z M349.219,62.5c-8.625-6.219-17.844-11.625-27.625-16.063l16.563-28.719l27.688,16 \
  L349.219,62.5z M97.688,208H64v-32h33.625C97.063,181.344,96,186.5,96,192C96,197.438,97.063,202.688,97.688,208z M448,176v32 \
  h-33.688c0.625-5.313,1.688-10.563,1.688-16c0-5.5-1.063-10.656-1.625-16H448z M401.219,257.375l29.063,16.75l-16,27.719 \
  l-26.188-15.125C392.813,277.063,397.188,267.313,401.219,257.375z M110.656,257.438c3.969,9.938,8.344,19.75,13.031,29.438 \
  l-25.969,14.969l-16-27.719L110.656,257.438z\"/> \
</svg> \
</div> \
                                    <div class=\"button\"> \
                                    <button onclick=\"a_on()\">On</button> <br/>\
                                    <button onclick=\"a_off()\">Off</button> \
                                    \
                                    </div>    \
                                    </div>    \
                                    <script type=\"application/javascript\"> \
                                    function a_on(){ \
var xhr = new XMLHttpRequest(); \
xhr.open('GET', '/command?flash=on', false); \
xhr.send(); \
if (xhr.status != 200) { \
  \
} else { \
  \
} \
 return false; \ 
                                    } \
                                    function a_off(){ \
var xhr = new XMLHttpRequest(); \
xhr.open('GET', '/command?flash=off', false); \
xhr.send(); \
if (xhr.status != 200) { \
  \
} else { \
  \
} \
return false; \ 
                                    } \
                                    </script> \
                                    <style type=\"text/css\"> \
                                    .com_block{text-align: center;}\
                                    </style> \
                                   </div></body></html>"); 
}

// What to do when a client requests "http://<IP>/command"
// This function doesn't care whether it is a GET or POST at the moment, 
// it will treat them the same just to make things easier.
void HandleCommand() {
    for (uint8_t i=0; i<server.args(); i++) {
        if (server.argName(i) == "flash") {
            if (server.arg(i) == "on") {
                FlashLeds();
                server.send(200, "text/plain", "LEDs turned on");
            } else {
                TurnOffLeds();
                server.send(200, "text/plain", "LEDs turned off");
            }
        }
    }
    server.send(404, "text/plain", "Command not found");
}

void FlashLeds() {
    // Flash LEDs or something then exit this func so that the webserver carries on running.
    Serial.println("LEDs on.");
    analogWrite(analogMotor, 180);
}

void TurnOffLeds() {
    // Flash LEDs or something then exit this func so that the webserver carries on running.
    Serial.println("LEDs off.");
    analogWrite(analogMotor, 0);
}

// If the route is not found then default to printing out what was sent to this server to aid in debug.
void HandleNotFound() {
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i=0; i<server.args(); i++) {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);}

void setup(void) {
    pinMode(analogMotor, OUTPUT); 

    Serial.begin(115200);
    WiFi.begin(ssid, password);
    Serial.println("Setting up.");

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println(".");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    if (MDNS.begin("esp8266")) {
        Serial.println("MDNS responder started");
    }

    //I left this in commented out just to show this method as well.
    server.on("/inline", []() {
        server.send(200, "text/plain", "this works as well");
    });
    
    analogWrite(analogMotor, 0);
    // Setup server routes.
    server.on("/", HandleRoot);
    server.on("/command", HandleCommand);
    server.onNotFound(HandleNotFound);
    server.begin();
    Serial.println("HTTP server started");
}

void loop(void) {
    server.handleClient();
}
